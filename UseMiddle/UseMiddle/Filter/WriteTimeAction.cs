﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;

namespace UseMiddle.Filter
{
    public class WriteTimeAction : Attribute, IActionFilter
    {
        private Stopwatch stopwatch = new Stopwatch();
        public WriteTimeAction()
        {

        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
            
        }

        void IActionFilter.OnActionExecuted(ActionExecutedContext context)
        {
            

            stopwatch.Stop();

            // var s = stopwatch.ElapsedMilliseconds;

            // byte[] bytes = Encoding.UTF8.GetBytes(s.ToString());

            // byte [] tempBytes=new byte[1024];
            //int count= context.HttpContext.Response.Body.Read(tempBytes,0, tempBytes.Length);

            //if (count > 0)
            //{

            //}
            // context.HttpContext.Response.Body.Write(bytes, 0, bytes.Length);


            context.HttpContext.Response.OnStarting(state =>
            {
                context.HttpContext.Response.Cookies.Append("causeTime", stopwatch.ElapsedMilliseconds.ToString()+"秒");
                return Task.FromResult(0);
            }, context.HttpContext);
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            stopwatch.Start();
        }
    }
}
